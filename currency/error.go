package currency

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// AbortWithError is response with error message
func abortWithError(c *gin.Context, code int, message string) {
	c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
		"error": gin.H{
			"code":    code,
			"message": message,
		},
	})
}
