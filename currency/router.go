package currency

import (
	"github.com/gin-gonic/gin"
)

// InitRouter create api routing
func InitRouter() *gin.Engine {
	router := gin.Default()

	router.GET("/currency", List)

	router.GET("/currency/:name", Show)

	router.POST("/currency", Store)

	router.PUT("/currency/:name", Update)

	router.DELETE("/currency/:name", Delete)

	return router
}
