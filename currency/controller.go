package currency

import (
	"currency/storage"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// List is get all resources.
func List(c *gin.Context) {
	var currencies []storage.Currency

	currencies, _ = StatStorage.All()

	c.JSON(http.StatusOK, gin.H{
		"currency": currencies,
	})
}

// Show is get specific resource.
func Show(c *gin.Context) {
	name := c.Param("name")

	currency, _ := StatStorage.Find(name)

	if currency.Name == "" {
		abortWithError(c, 404, fmt.Sprintf("Currency: %s not found.", name))
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"currency": currency,
	})
}

// Store is create resource.
func Store(c *gin.Context) {
	var currency storage.Currency
	if err := c.ShouldBind(&currency); err != nil {
		abortWithError(c, 400, err.Error())
		return
	}

	if currency.Name == "" {
		abortWithError(c, 422, fmt.Sprintf("Param: `name` must be required."))
		return
	}

	valid, _ := StatStorage.Find(currency.Name)
	if valid.Name != "" {
		abortWithError(c, 400, fmt.Sprintf("Currency: %s exists.", currency.Name))
		return
	}

	jsonCurrency, _ := json.Marshal(currency)
	if err := StatStorage.Create(currency.Name, string(jsonCurrency)); err != nil {
		abortWithError(c, 400, err.Error())
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
	})
}

// Update is change specific resource value.
func Update(c *gin.Context) {
	name := c.Param("name")
	currency, _ := StatStorage.Find(name)

	if currency.Name == "" {
		abortWithError(c, 404, fmt.Sprintf("Currency: %s not found.", name))
		return
	}

	if val, ok := c.GetPostForm("cash_buying"); ok && val != "" {
		f64, err := strconv.ParseFloat(val, 64)
		if err != nil {
			abortWithError(c, 422, fmt.Sprintf("param cash_buying invalid: %s", err.Error()))
			return
		}

		currency.Cash.Buying = f64
	}

	if val, ok := c.GetPostForm("cash_selling"); ok && val != "" {
		f64, err := strconv.ParseFloat(val, 64)
		if err != nil {
			abortWithError(c, 422, fmt.Sprintf("param cash_selling invalid: %s", err.Error()))
			return
		}
		currency.Cash.Selling = f64
	}

	if val, ok := c.GetPostForm("spot_buying"); ok && val != "" {
		f64, err := strconv.ParseFloat(val, 64)
		if err != nil {
			abortWithError(c, 422, fmt.Sprintf("param spot_buying invalid: %s", err.Error()))
			return
		}
		currency.Spot.Buying = f64
	}

	if val, ok := c.GetPostForm("spot_selling"); ok && val != "" {
		f64, err := strconv.ParseFloat(val, 64)
		if err != nil {
			abortWithError(c, 422, fmt.Sprintf("param spot_selling invalid: %s", err.Error()))
			return
		}
		currency.Spot.Selling = f64
	}

	jsonCurrency, _ := json.Marshal(currency)
	if err := StatStorage.Update(currency.Name, string(jsonCurrency)); err != nil {
		abortWithError(c, 400, err.Error())
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
	})
}

// Delete is remove specific resource.
func Delete(c *gin.Context) {
	name := c.Param("name")

	if err := StatStorage.Destroy(name); err != nil {
		abortWithError(c, 400, err.Error())
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
	})
}
