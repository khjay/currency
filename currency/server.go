package currency

// RunHTTPServer will start a RESTful API server with routing.
func RunHTTPServer() error {
	router := InitRouter()

	err := router.Run(":" + CurrencyConf.Core.Port)
	if err != nil {
		LogAccess.Fatalf("Run HTTP Server failed: %s", err.Error())
	}

	return err
}
