package currency

import (
	"currency/config"
	"currency/storage"

	"github.com/sirupsen/logrus"
)

var (
	// CurrencyConf is Currency config
	CurrencyConf config.ConfYaml

	// LogAccess is log server request log
	LogAccess *logrus.Logger

	// LogError is log server error log
	LogError *logrus.Logger

	// StatStorage is implements database storage
	StatStorage storage.Storage
)
