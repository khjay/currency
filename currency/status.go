package currency

import "currency/storage/redis"

// InitAppStatus was init status of storage engine
func InitAppStatus() error {
	LogAccess.Infof("Init Status Engine as: %s", CurrencyConf.Stat.Engine)

	switch engine := CurrencyConf.Stat.Engine; engine {
	case "redis":
		StatStorage = redis.New(CurrencyConf)
	default:
		LogAccess.Fatalf("Status Engine: %s not support", engine)
	}

	return StatStorage.Init()
}
