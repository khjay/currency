package currency

import (
	"currency/config"
	"currency/storage"
	storageRedis "currency/storage/redis"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/appleboy/gofight"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type RefreshRoutingTestSuite struct {
	suite.Suite
	Request *gofight.RequestConfig
}

func (suite *RefreshRoutingTestSuite) SetupTest() {
	config, _ := config.LoadConf("")

	StatStorage = storageRedis.New(config)
	err := StatStorage.Init()

	assert.Nil(suite.T(), err)

	client := redis.NewClient(&redis.Options{
		Addr:     config.Stat.Redis.Addr,
		Password: config.Stat.Redis.Password,
		DB:       config.Stat.Redis.DB,
	})

	client.Del("currency")

	suite.Request = gofight.New()
}

func TestRedisRefreshTestSuite(t *testing.T) {
	suite.Run(t, new(RefreshRoutingTestSuite))
}

func (suite *RefreshRoutingTestSuite) TestEmptyCurrencyList() {
	suite.Request.GET("/currency").
		SetDebug(true).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), "{\"currency\":[]}", r.Body.String())
			assert.Equal(suite.T(), http.StatusOK, r.Code)
		})
}

func (suite *RefreshRoutingTestSuite) TestNonEmptyCurrencyList() {
	currency := newCurrency("TWD", 10, 20, 30, 40)
	StatStorage.Create("TWD", currency)

	expect := fmt.Sprintf("{\"currency\":[%s]}", currency)

	suite.Request.GET("/currency").
		SetDebug(true).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), expect, r.Body.String())
			assert.Equal(suite.T(), http.StatusOK, r.Code)
		})
}

func (suite *RefreshRoutingTestSuite) TestNonExistsCurrencyWillReturn404() {
	expect := "{\"error\":{\"code\":404,\"message\":\"Currency: TWD not found.\"}}"

	suite.Request.GET("/currency/TWD").
		SetDebug(true).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), expect, r.Body.String())
			assert.Equal(suite.T(), http.StatusBadRequest, r.Code)
		})
}

func (suite *RefreshRoutingTestSuite) TestCanGetExistsCurrency() {
	currency := newCurrency("TWD", 10, 20, 30, 40)
	StatStorage.Create("TWD", currency)

	expect := "{\"currency\":{\"name\":\"TWD\",\"cash\":{\"buying\":10,\"selling\":20},\"spot\":{\"buying\":30,\"selling\":40}}}"

	suite.Request.GET("/currency/TWD").
		SetDebug(true).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), expect, r.Body.String())
			assert.Equal(suite.T(), http.StatusOK, r.Code)
		})
}

func (suite *RefreshRoutingTestSuite) TestStoredCurrencyMustProvideNameParam() {
	expect := "{\"error\":{\"code\":422,\"message\":\"Param: `name` must be required.\"}}"

	suite.Request.POST("/currency").
		SetDebug(true).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), expect, r.Body.String())
			assert.Equal(suite.T(), http.StatusBadRequest, r.Code)
		})
}

func (suite *RefreshRoutingTestSuite) TestStoredCurrencyMustBeUnique() {
	currency := newCurrency("TWD", 10, 20, 30, 40)
	StatStorage.Create("TWD", currency)

	expect := "{\"error\":{\"code\":400,\"message\":\"Currency: TWD exists.\"}}"

	suite.Request.POST("/currency").
		SetDebug(true).
		SetForm(gofight.H{
			"name": "TWD",
		}).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), expect, r.Body.String())
			assert.Equal(suite.T(), http.StatusBadRequest, r.Code)
		})
}

func (suite *RefreshRoutingTestSuite) TestCanStoreCurrency() {
	expect := "{\"success\":true}"

	suite.Request.POST("/currency").
		SetDebug(true).
		SetForm(gofight.H{
			"name": "TWD",
		}).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), expect, r.Body.String())
			assert.Equal(suite.T(), http.StatusOK, r.Code)
		})
}

func (suite *RefreshRoutingTestSuite) TestNonExistsCurrencyCanNotUpdate() {
	expect := "{\"error\":{\"code\":404,\"message\":\"Currency: TWD not found.\"}}"
	suite.Request.PUT("/currency/TWD").
		SetDebug(true).
		SetForm(gofight.H{
			"name": "TWD",
		}).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), expect, r.Body.String())
			assert.Equal(suite.T(), http.StatusBadRequest, r.Code)
		})
}

func (suite *RefreshRoutingTestSuite) TestCanUpdateCurrency() {
	currency := newCurrency("TWD", 10, 20, 30, 40)
	StatStorage.Create("TWD", currency)

	expect := "{\"success\":true}"

	suite.Request.PUT("/currency/TWD").
		SetDebug(true).
		SetForm(gofight.H{
			"name":         "TWD",
			"cash_buying":  "25",
			"cash_selling": "26",
			"spot_buying":  "35",
			"spot_selling": "36",
		}).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), expect, r.Body.String())
			assert.Equal(suite.T(), http.StatusOK, r.Code)
		})

	expectCurrency := newCurrency("TWD", 25, 26, 35, 36)
	actualCurrency, _ := StatStorage.Find("TWD")
	jsonCurrency, _ := json.Marshal(actualCurrency)

	assert.Equal(suite.T(), expectCurrency, string(jsonCurrency))
}

func (suite *RefreshRoutingTestSuite) TestCanDeleteNonExistsCurrency() {
	expect := "{\"success\":true}"

	suite.Request.DELETE("/currency/TWD").
		SetDebug(true).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), expect, r.Body.String())
			assert.Equal(suite.T(), http.StatusOK, r.Code)
		})
}

func (suite *RefreshRoutingTestSuite) TestCanDeleteExistsCurrency() {
	currency := newCurrency("TWD", 10, 20, 30, 40)
	StatStorage.Create("TWD", currency)

	expect := "{\"success\":true}"

	suite.Request.DELETE("/currency/TWD").
		SetDebug(true).
		Run(ginEngine(), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(suite.T(), expect, r.Body.String())
			assert.Equal(suite.T(), http.StatusOK, r.Code)
		})

	actualCurrency, _ := StatStorage.Find("TWD")
	assert.Equal(suite.T(), "", actualCurrency.Name)
}

func ginEngine() *gin.Engine {
	gin.SetMode(gin.TestMode)
	router := gin.New()

	router.GET("/currency", List)
	router.GET("/currency/:name", Show)
	router.POST("/currency", Store)
	router.PUT("/currency/:name", Update)
	router.DELETE("/currency/:name", Delete)

	return router
}

func newCurrency(name string, cashBuying float64, cashSelling float64, spotBuying float64, spotSelling float64) string {
	currency := &storage.Currency{
		Name: name,
		Cash: storage.CashRate{
			Buying:  cashBuying,
			Selling: cashSelling,
		},
		Spot: storage.SpotRate{
			Buying:  spotBuying,
			Selling: spotSelling,
		},
	}

	result, _ := json.Marshal(currency)

	return string(result)
}
