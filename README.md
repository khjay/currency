# Golang Currency

## Contents

- [Description](#description)
- [Quick Start](#quick-start)
- [Response Format](#request-format)
- [RESTful API](#restful-api)
- [Postman](#postman)

## Description

前端需要呈現如圖的相關資料，請使用 `Golang` 實作的相關 `RESTful API`，需要以下幾個功能：

1. 查看現有的所有的幣別、現金匯率（買入、賣出）、即期匯率（買入、賣出）

2. 查看特定幣別的現金匯率（買入、賣出）、即期匯率（買入、賣出）

3. 新增新的幣別、現金匯率（買入、賣出）、即期匯率（買入、賣出）

4. 更新既有的幣別匯率紀錄

5. 刪除既有的幣別匯率紀錄

## Quick Start

1. Install dependencies which needed:
```
$ govendor sync
```

2. Setting your environment: `config/config.yml`

3. Run it:
```
$ go run main.go -c config/config.yml
```

## Response Format

### Success Response

Success response will get http status code 200, then following format:

```json
{
  "currency": {}
}
```

### Error Response

Error response will get http status code 400, then with following format:

```json
{
  "error": {
    "code": 1,
    "message": 2
  }
}
```

## RESTful API

Currency support the following API.

* **GET** `/currency` Show all currencies.
* **GET**  `/currency/:name` Show specific currency by name.
* **POST**  `/currency` Create new currency.
* **PUT**  `/currency/:name` Update specific currency by name.
* **DELETE** `/currency/:name` Delete specific currency by name.

### GET /currency

Show all currencies.

```json
{
  "currency": [
    {
      "name": "USD",
      "cash": {
        "buying": 123,
        "selling": 105
      },
      "spot": {
        "buying": 200,
        "selling": 13
      }
    },
    {
      "name": "TWD",
      "cash": {
        "buying": 30,
        "selling": 35
      },
      "spot": {
        "buying": 28,
        "selling": 28
      }
    }
  ]
}
```

### GET /currency/:name

Show specific currency by name.

```json
{
  "currency": {
    "name": "USD",
    "cash": {
      "buying": 123,
      "selling": 105
    },
    "spot": {
      "buying": 200,
      "selling": 13
    }
  }
}
```

### POST /currency

Create new currency.

Form Data:

| name          | type    | description    | required |
|---------------|---------|----------------|----------|
| name          | string  | 幣別名稱         | required |
| cash_buying   | float64 | 現金匯率 本行買入 | optional |
| cash_selling  | float64 | 現金匯率 本行賣出 | optional |
| spot_buying   | float64 | 即期匯率 本行買入 | optional |
| spot_selling  | float64 | 即期匯率 本行賣出 | optional |

```json
{
  "success": true
}
```

### PUT /currency/:name

Update specific currency by name.

Form Data:

| name          | type    | description    | required |
|---------------|---------|----------------|----------|
| cash_buying   | float64 | 現金匯率 本行買入 | optional |
| cash_selling  | float64 | 現金匯率 本行賣出 | optional |
| spot_buying   | float64 | 即期匯率 本行買入 | optional |
| spot_selling  | float64 | 即期匯率 本行賣出 | optional |

```json
{
  "success": true
}
```

### DELETE /currency/:name

Delete specific currency by name.

```json
{
  "success": true
}
```

## Postman

You can also download sample request by following [link](https://www.getpostman.com/collections/67758d21224c8d721846).

Then import it to postman.
