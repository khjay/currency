FROM golang:1.11-stretch

RUN apt-get update && \
    apt-get install git && \
    go get -u github.com/kardianos/govendor

WORKDIR $GOPATH/src/currency

CMD ["go", "run", "main.go", "-c", "config/config.yml"]
