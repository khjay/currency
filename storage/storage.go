package storage

// Storage interface
type Storage interface {
	Init() error
	All() ([]Currency, error)
	Create(name string, data string) error
	Find(name string) (Currency, error)
	Update(name string, updatedCurrency string) error
	Destroy(name string) error
}

// Currency structure
type Currency struct {
	Name string   `form:"name" json:"name"`
	Cash CashRate `json:"cash"`
	Spot SpotRate `json:"spot"`
}

// CashRate is sub structure of Currency
type CashRate struct {
	Buying  float64 `form:"cash_buying" json:"buying"`
	Selling float64 `form:"cash_selling" json:"selling"`
}

// SpotRate is sub structure of Currency
type SpotRate struct {
	Buying  float64 `form:"spot_selling" json:"buying"`
	Selling float64 `form:"spot_selling" json:"selling"`
}
