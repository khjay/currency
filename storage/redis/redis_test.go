package redis

import (
	"currency/config"
	"currency/storage"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type RedisRefreshTestSuite struct {
	suite.Suite
	RedisStorage *Storage
}

func (suite *RedisRefreshTestSuite) SetupTest() {
	config, _ := config.LoadConf("")

	suite.RedisStorage = New(config)
	err := suite.RedisStorage.Init()

	assert.Nil(suite.T(), err)

	client.Del("currency")
}

func TestRedisRefreshTestSuite(t *testing.T) {
	suite.Run(t, new(RedisRefreshTestSuite))
}

func TestRedisConnectError(t *testing.T) {
	config, _ := config.LoadConf("")
	config.Stat.Redis.Addr = "invalid:address"

	redis := New(config)
	err := redis.Init()

	assert.NotNil(t, err)
}

func (suite *RedisRefreshTestSuite) TestCanCreateCurrency() {
	currency := newCurrency("TWD", 10, 11, 12, 13)

	suite.RedisStorage.Create("TWD", currency)

	actual, _ := client.HGet("currency", "TWD").Result()
	assert.Equal(suite.T(), currency, actual)
}

func (suite *RedisRefreshTestSuite) TestCanFindCurrency() {
	currency := newCurrency("TWD", 10, 11, 12, 13)
	client.HSet("currency", "TWD", currency)

	v, _ := suite.RedisStorage.Find("TWD")
	jsonCurrency, _ := json.Marshal(v)
	assert.Equal(suite.T(), currency, string(jsonCurrency))
}

func (suite *RedisRefreshTestSuite) TestCanGetAllCurrency() {
	client.HSet("currency", "TWD", newCurrency("TWD", 10, 11, 12, 13))

	expected := make([]storage.Currency, 1)
	expected[0] = storage.Currency{
		Name: "TWD",
		Cash: storage.CashRate{
			Buying:  10,
			Selling: 11,
		},
		Spot: storage.SpotRate{
			Buying:  12,
			Selling: 13,
		},
	}

	actual, _ := suite.RedisStorage.All()
	assert.Equal(suite.T(), expected, actual)
}

func (suite *RedisRefreshTestSuite) TestCanDestroyCurrency() {
	client.HSet("currency", "TWD", newCurrency("TWD", 10, 11, 12, 13))

	suite.RedisStorage.Destroy("TWD")

	result := client.HGet("currency", "TWD").Val()
	assert.Equal(suite.T(), "", result)
}

func (suite *RedisRefreshTestSuite) TestCanUpdateValidCurrency() {
	client.HSet("currency", "TWD", newCurrency("TWD", 10, 11, 12, 13))

	updatedCurrency := newCurrency("TWD", 20, 30, 40, 50)
	suite.RedisStorage.Update("TWD", updatedCurrency)

	result := client.HGet("currency", "TWD").Val()
	assert.Equal(suite.T(), newCurrency("TWD", 20, 30, 40, 50), result)
}

func newCurrency(name string, cashBuying float64, cashSelling float64, spotBuying float64, spotSelling float64) string {
	currency := &storage.Currency{
		Name: name,
		Cash: storage.CashRate{
			Buying:  cashBuying,
			Selling: cashSelling,
		},
		Spot: storage.SpotRate{
			Buying:  spotBuying,
			Selling: spotSelling,
		},
	}

	result, _ := json.Marshal(currency)

	return string(result)
}
