package redis

import (
	"currency/config"
	"currency/storage"
	"encoding/json"

	"github.com/go-redis/redis"
)

var client *redis.Client

// Storage is an interface
type Storage struct {
	config config.ConfYaml
}

// New implements Storage interface
func New(config config.ConfYaml) *Storage {
	return &Storage{
		config: config,
	}
}

// Init will prepare redis client
func (s *Storage) Init() error {
	client = redis.NewClient(&redis.Options{
		Addr:     s.config.Stat.Redis.Addr,
		Password: s.config.Stat.Redis.Password,
		DB:       s.config.Stat.Redis.DB,
	})

	_, err := client.Ping().Result()
	if err != nil {
		return err
	}

	return nil
}

// All return all of the resources
func (s *Storage) All() ([]storage.Currency, error) {
	var currencies []storage.Currency

	m, err := client.HGetAll("currency").Result()
	if err != nil {
		return currencies, err
	}

	currencies = make([]storage.Currency, 0, len(m))

	for _, v := range m {
		currency := storage.Currency{}

		json.Unmarshal([]byte(v), &currency)
		currencies = append(currencies, currency)
	}

	return currencies, nil
}

// Create insert a new record in the redis
func (s *Storage) Create(name string, data string) error {
	return client.HSet("currency", name, data).Err()
}

// Find retrieve a resource by key
func (s *Storage) Find(name string) (storage.Currency, error) {
	var currency storage.Currency
	data, err := client.HGet("currency", name).Result()
	if err != nil {
		return currency, err
	}

	json.Unmarshal([]byte(data), &currency)

	return currency, nil
}

// Update can set any attributes you wish
func (s *Storage) Update(name string, updatedCurrency string) error {
	return client.HSet("currency", name, updatedCurrency).Err()
}

// Destroy delete specific resource
func (s *Storage) Destroy(name string) error {
	return client.HDel("currency", name).Err()
}
