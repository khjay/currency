package main

import (
	"currency/config"
	"currency/currency"
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	var (
		configFile string
	)

	flag.StringVar(&configFile, "c", "", "Configuration file path.")
	flag.StringVar(&configFile, "config", "", "Configuration file path.")
	flag.Usage = usage

	flag.Parse()

	var err error

	currency.CurrencyConf, err = config.LoadConf(configFile)
	if err != nil {
		log.Fatalf("Load yaml file error: '%v'", err)
	}

	if err = currency.InitLog(); err != nil {
		log.Fatalf("Can't load log module, error: %v", err)
	}

	if err = currency.InitAppStatus(); err != nil {
		currency.LogError.Fatalf("Run HTTP Server failed: %s", err.Error())
	}

	currency.RunHTTPServer()

}

var usageStr = `
Usage: currency [options]

Server Options:
    -c, --config <file>        Configuration file path
Common Options:
    -h, --help                 Show this message
`

func usage() {
	fmt.Printf("%s\n", usageStr)
	os.Exit(0)
}
