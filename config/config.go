package config

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/spf13/viper"
)

var defaultConf = []byte(`
core:
  port: "8080"
  mode: "debug"

log:
  format: "string"
  access_log: "stdout"
  access_level: "debug"
  error_log: "stderr"
  error_level: "error"

stat:
  engine: "redis"
  redis:
    addr: "localhost:6379"
    password: ""
    db: 0
`)

// ConfYaml is config structure
type ConfYaml struct {
	Core SectionCore `yaml:"core"`
	Log  SectionLog  `yaml:"log"`
	Stat SectionStat `yaml:"stat"`
}

// SectionCore is sub section of config
type SectionCore struct {
	Port string `yaml:"port"`
	Mode string `yaml:"mode"`
}

// SectionLog is sub section of config
type SectionLog struct {
	Format      string `yaml:"format"`
	AccessLog   string `yaml:"access_log"`
	AccessLevel string `yaml:"access_level"`
	ErrorLog    string `yaml:"error_log"`
	ErrorLevel  string `yaml:"error_level"`
}

// SectionStat is sub section of config
type SectionStat struct {
	Engine string       `yaml:"engine"`
	Redis  SectionRedis `yaml:"redis"`
}

// SectionRedis is sub section of config
type SectionRedis struct {
	Addr     string `yaml:"addr"`
	Password string `yaml:"password"`
	DB       int    `yaml:"db"`
}

// LoadConf can load config from yaml file.
func LoadConf(confPath string) (ConfYaml, error) {
	var conf ConfYaml

	viper.SetConfigType("yaml")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if confPath != "" {
		content, err := ioutil.ReadFile(confPath)

		if err != nil {
			return conf, err
		}

		viper.ReadConfig(bytes.NewBuffer(content))
	} else {
		viper.AddConfigPath(".")
		viper.SetConfigName("config")

		if err := viper.ReadInConfig(); err == nil {
			fmt.Println("Using config file:", viper.ConfigFileUsed())
		} else {
			fmt.Println("Read config from DefaultConf")
			viper.ReadConfig(bytes.NewBuffer(defaultConf))
		}
	}

	conf.Core.Mode = viper.GetString("core.mode")
	conf.Core.Port = viper.GetString("core.port")

	conf.Log.Format = viper.GetString("log.format")
	conf.Log.AccessLog = viper.GetString("log.access_log")
	conf.Log.AccessLevel = viper.GetString("log.access_level")
	conf.Log.ErrorLog = viper.GetString("log.error_log")
	conf.Log.ErrorLevel = viper.GetString("log.error_level")

	conf.Stat.Engine = viper.GetString("stat.engine")

	conf.Stat.Redis.Addr = viper.GetString("stat.redis.addr")
	conf.Stat.Redis.Password = viper.GetString("stat.redis.password")
	conf.Stat.Redis.DB = viper.GetInt("stat.redis.db")

	return conf, nil
}
