package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// Test file is missing
func TestMissingFile(t *testing.T) {
	_, err := LoadConf("404.yml")

	assert.NotNil(t, err)
}

func TestValidateConf(t *testing.T) {
	conf, err := LoadConf("config.yml")

	if err != nil {
		panic("failed to load config.yml from file")
	}

	assert.Equal(t, "8080", conf.Core.Port)
	assert.Equal(t, "debug", conf.Core.Mode)

	assert.Equal(t, "string", conf.Log.Format)
	assert.Equal(t, "stdout", conf.Log.AccessLog)
	assert.Equal(t, "debug", conf.Log.AccessLevel)
	assert.Equal(t, "stderr", conf.Log.ErrorLog)
	assert.Equal(t, "error", conf.Log.ErrorLevel)

	assert.Equal(t, "redis", conf.Stat.Engine)
	assert.Equal(t, "", conf.Stat.Redis.Password)
	assert.Equal(t, 0, conf.Stat.Redis.DB)
}

func TestValidateDefaultConf(t *testing.T) {
	conf, err := LoadConf("")

	if err != nil {
		panic("failed to load config.yml from file")
	}

	assert.Equal(t, "8080", conf.Core.Port)
	assert.Equal(t, "debug", conf.Core.Mode)

	assert.Equal(t, "string", conf.Log.Format)
	assert.Equal(t, "stdout", conf.Log.AccessLog)
	assert.Equal(t, "debug", conf.Log.AccessLevel)
	assert.Equal(t, "stderr", conf.Log.ErrorLog)
	assert.Equal(t, "error", conf.Log.ErrorLevel)

	assert.Equal(t, "redis", conf.Stat.Engine)
	assert.Equal(t, "", conf.Stat.Redis.Password)
	assert.Equal(t, 0, conf.Stat.Redis.DB)
}
